<?php

    //require ("animal.php");
    require("frog.php & ape.php");

    $sheep = new Animal("shaun");

    echo "Nama hewan  : $sheep->name <br>"; // "shaun"
    echo "Jumlah kaki : $sheep->legs <br>"; // 4
    echo "Cold blooded : $sheep->cold_blooded <br><br>"; // "no"

    $kodok = new Frog("buduk");

    echo "Nama hewan  : $kodok->name <br>";
    echo "Jumlah kaki : $kodok->legs <br>";
    echo "Cold blooded : $kodok->cold_blooded <br>";
    echo $kodok->jump();

    echo "<br><br>";

    $sungokong = new Ape("kera sakti");

    echo "Nama hewan  : $sungokong->name <br>";
    echo "Jumlah kaki : $sungokong->legs <br>";
    echo "Cold blooded : $sungokong->cold_blooded <br>";
    echo $sungokong->yell();
